//
//  AppDelegate.swift
//  iOSTemplate
//
//  Created by John Hampton on 2/24/19.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    /// アプリケーションが起動した後のカスタマイズポイント
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        return true
    }

    /// アプリケーションがアクティブから非アクティブに移行する直前に呼ばれる
    func applicationWillResignActive(_ application: UIApplication) {
        // ここで進行中のタスクを一時停止したり、タイマーを無効にしたりします
    }

    /// アプリケーションがバックグラウンドに入ったときに呼ばれる
    func applicationDidEnterBackground(_ application: UIApplication) {
        // ここで共有リソースを解放したり、ユーザーデータを保存したりします
    }

    /// アプリケーションがバックグラウンドからアクティブ状態に移行するときに呼ばれる
    func applicationWillEnterForeground(_ application: UIApplication) {
        // ここでバックグラウンドに入るときに行った変更を元に戻します
    }

    /// アプリケーションがアクティブになったときに呼ばれる
    func applicationDidBecomeActive(_ application: UIApplication) {
        // ここで一時停止していたタスクを再開します
    }

    /// アプリケーションが終了する直前に呼ばれる
    func applicationWillTerminate(_ application: UIApplication) {
        // ここで適切な場合はデータを保存します
    }
}
